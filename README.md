# README #

Zum Starten der Anwendung muss lediglich startscreen.html geöffnet werden.
Damit die Daten aufgerufen werden können, benötigt man eine Internetverbindung.

Möchte man den Aufruf der Quizfragen aus dem quizfragen.json über jquery testen, befindet sich ein auskommentierter Code im quiz.js. In der Abgabe wurde die json Datei in eine javascript Datei eingefügt, da sonst der Aufruf über Chrome teilweise nicht funktioniert (Wie wir nachgelesen haben, unterstützt Chrome den Aufruf lokaler json-Dateien nicht).