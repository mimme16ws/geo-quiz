var MemoryGame = MemoryGame || {};
MemoryGame.MemoryGameBoard = function (onCardSolved) {
    "use strict";
    
    /* eslint-env browser */
    
    var that = {},
        informScore = onCardSolved,
        board,
        deck = ["italian","art","monument","paris","greek","moschee","building","ballon","newyork","italian","art","monument","paris","greek","moschee","building","ballon","newyork"];

    // for the shuffle; Source: http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript
    (function () {
        "use strict";
         /*eslint-disable*/
         Array.prototype.shuffle = function () {
        for (var j, x, i = this.length; i; j = Math.floor(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
        };
         /*eslint-enable*/
    }());
    
    
    //check if open cards are the same
    function checkBothCards(openCards) {
        var firstCard, secondCard;
        firstCard = openCards[0].getAttribute("item");
        secondCard = openCards[1].getAttribute("item");
        
        if (firstCard === secondCard) {
            return true;
        } else {
            return false;
        }
    }
    

    //cards are closed
    function closeCard(card) {
        setTimeout(function () {
            card.classList.toggle("open");
        }, 510);
    }
    
    
    //mark card as solved: cards turn green 
    function solveCard(card, listenerToRemove) {
        card.removeEventListener("click", listenerToRemove, false);
        card.classList.remove("open");
        card.classList.add("solved");
    }
    
    
    //the points are from the length of the solved cards and are send to MemoryGame
    function showScore() {
        var cardsSolved = document.querySelectorAll("#board .solved").length;
        informScore(cardsSolved);
    }

    function onCardClicked(event) {
        var openCards, target;
        
        target = event.target;
        target.classList.toggle("open");
        openCards = document.querySelectorAll("#board .open");
        
        if (openCards.length === 2) {
            if (checkBothCards(openCards) === true) {
                solveCard(openCards[0], onCardClicked);
                solveCard(openCards[1], onCardClicked);
                showScore();
            } else {
                closeCard(openCards[0]);
                closeCard(openCards[1]);
            }
        }
    }
    
   
    function init() {
        var card, i;
        deck.shuffle();
        for (i=0; i < deck.length; i++) {
            card = document.createElement("span");
            card.classList.add("card");
            card.setAttribute("item", deck[i]);
            card.style.backgroundImage = "url('res/images/" + deck[i] + ".png')";
            
            card.addEventListener("click", onCardClicked, false);
            board = document.querySelector("#board");
            board.appendChild(card);
        }
       
    }

    that.init = init;
    return that;
    
};



