var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.Main = (function() {
    "use strict";
    
    /* eslint-env browser  */


    var that = {},
        mapView,
        mapController;
       
    function init(){
        mapView = GeoQuizMain.MapView;
        mapView.init();
        mapController = GeoQuizMain.MapController;
        mapController.init();
    }

    that.init = init;
    return that;
}());
