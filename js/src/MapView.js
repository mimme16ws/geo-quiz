var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.MapView = (function(){
    "use strict";
    
    /* eslint-env browser  */


    
    var that = {},
        continentMap,
        germanyMap,
        europeMap,
        map,
        languageAudio,
        hintField;
       
    
    function init(){ 
        continentMap = GeoQuizMain.geoquiz;
        europeMap = GeoQuizMain.EuropeMap;
        germanyMap = GeoQuizMain.GermanyMap;
        checkOpenMap();
        hintField = document.querySelector("#hintObject");
    }
    
    
    /*display image or play sound as hint
    */
    function displayHintData(hint){
        var url,
            image;
        url = hint["_url"];
        if (url.indexOf(".mpeg")> 0 || url.indexOf(".mp3") > 0){
            languageAudio = new Audio(url);
            hintField.appendChild(languageAudio);
            languageAudio.play();
        }else{
            image = document.createElement("img");
            image.src = url;
            hintField.appendChild(image);
        }
    }
    
   
    
    function displayHintText(text){
        hintField.innerHTML = text; 
    }
    
    function clearHintField(){
        hintField.innerHTML = "";
    }
    
    function removeHintData(){
        if (hintField.hasChildNodes()){
            hintField.removeChild(hintField.childNodes);
        }
    }
    
    
    /*checks which map (continents, countries or germany) is used*/
    function checkOpenMap(){
        if (continentMap != undefined){
            map = continentMap;
        }
        if (europeMap != undefined){
            map = europeMap;
        }
        if (germanyMap != undefined){
            map = germanyMap;
        }
        
    }
    
    function clickedLocation(id){
        map.clickedLocation(id);   
    }
    
    function inputColor(id){
        map.inputColor(id);   
    }
    
    function locationSetColor(correct, id){
        map.setColor(correct,id);
        
    }
    
    function setClick(id){
        map.setClick(id);
    }
    
    that.setClick = setClick; 
    that.removeHintData = removeHintData;
    that.clearHintField = clearHintField;
    that.displayHintText = displayHintText;
    that.displayHintData = displayHintData;
    that.clickedLocation = clickedLocation;
    that.inputColor = inputColor;
    that.locationSetColor = locationSetColor;
    that.init = init;
    return that;
}());


 
