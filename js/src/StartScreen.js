var StartScreen = StartScreen || {};

StartScreen = (function() {
    "use strict";
    /* eslint-env browser */

    var that = {},
        infoButton,
        closeButton,
        infoText,
        level2,
        level3,
        localScore,
        showScore,
        restartButton;    

    function init() {
        getElements();
        setScore();
        addEventListener();        
        checkScore();       
    }
    
     /*get the elements from the DOM*/
    function getElements(){
        infoButton = document.querySelector("#button-info");
        closeButton = document.querySelector(".close");
        restartButton = document.querySelector("#button-reset");  
        level2 = document.getElementById("level2");
        level3 = document.getElementById("level3");
    }
    
    /*set the value "score" from the localStorage*/
    function setScore(){
        if (localStorage.getItem("score") != null){
            localScore = localStorage.getItem("score");          
        }else{
            localScore = 0;
        }
        showScore = document.querySelector("#score");
        showScore.textContent = "Score: " + localScore;
    }
    
    function onCloseButtonClicked(){
        infoText.style.visibility ="hidden";        
    }
    
    function onButtonClicked(){
        infoText = document.querySelector("#overlay");
        infoText.style.visibility = "visible";
    } 
    
    /*restart the game and delete the localStorage*/
    function onRestartButtonClicked(){
        localStorage.clear();
        setScore();
        level2.classList.add("closed");
        level3.classList.add("closed");        
    }
    
    /*click listener for the info button, close button in the info message and the restart button*/
    function addEventListener(){
        infoButton.addEventListener("click", onButtonClicked);
        closeButton.addEventListener("click", onCloseButtonClicked);  
        restartButton.addEventListener("click", onRestartButtonClicked);        
    }
        
    /*check the score to unlock the next level when a certain score is reached*/
    function checkScore(){
        if (localStorage.getItem("score") > 40){
            level2.classList.remove("closed");
            level2.classList.add("card");
            level2.onclick = function() {
                return true;
            };
        }        
        if (localStorage.getItem("score") > 90){
            level3.classList.remove("closed");
            level3.classList.add("card");
            level3.onclick = function() {
                return true;
            };
        }
    }
    
    that.init = init;
    return that;    
}());