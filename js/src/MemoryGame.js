var MemoryGame = MemoryGame || {};
MemoryGame.Main = (function () {
    "use strict";
    
    /* eslint-env browser */
    
    var that = {},
        seconds = 80,
        stillPlaying = true,
        board,
        success,
        lost,
        text1,
        text2,
        scorememory,
        extrapoints;
    
    function init() {
        setScore();
        board = MemoryGame.MemoryGameBoard(onCardSolved);
        board.init();
        setInterval(setTime, 1000);
    }

    //display points
    function setScore(){
        if (localStorage.getItem("score") != null){
            scorememory = localStorage.getItem("score");
        }
        else{
            scorememory = 0;
        }
        document.querySelector("#score").textContent = "Score: " + scorememory;
    }
        
    
    //display information that the player lost
    function lostGame(){
        lost = document.getElementById("overlay2");
        stillPlaying = false;
        lost.style.visibility = "visible"; 
        text1 = document.getElementById("lostText");
        text1.innerHTML ="Du hast verloren! Aber du hast dir " + extrapoints + " zusätzliche Bonuspunkte geholt!";
    }
    
    //display information that the player won
    function finishGame() {
        success = document.querySelector("#overlay");
        stillPlaying = false;
        success.style.visibility = "visible";  
        text2 = document.getElementById("successText");
        text2.innerHTML = "Glückwunsch du hast gewonnen und hast dir " + extrapoints + " zusätzliche Bonuspunkte geholt!";
           
    }
     
    //current score calculated and displayed
    function updateScore(cardsSolved) {
        var points = 2;
        scorememory = parseInt(scorememory) + parseInt(points);
        localStorage.setItem("score", scorememory);
        setScore();
        extrapoints = cardsSolved;      
    }

    //timer 
    function setTime() {
        var minute, sec, time, zero = 0;
        time = document.querySelector("#time");
        
        if (!stillPlaying) {
            return;
        }
        seconds--;
        
        minute = Math.floor(seconds / 60);
        sec = Math.floor(seconds % 60);

        if (sec === 10 && minute === 0){
            time.style.color = "#F78181";
        }
        
        if (sec < 10) {
            sec = "0" + sec;
        } 

        time.innerHTML = "0" + minute + ":" + sec;
    
        if (minute === zero && sec === "0" + zero ){
            lostGame();
        
        }
    }

    function onCardSolved(cardsSolved) {
        var totalCards = 18;
        updateScore(cardsSolved);
        if (cardsSolved === totalCards) {
            finishGame();
        }
    }

   


    that.init = init;
    return that;

}());

