var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.LandmarkQuiz = (function() {
    "use strict";
    
    /* eslint-env browser  */


    var that = {},
        BOX_NUMBER = 160,
        ENTER_KEY_CODE = 13,
        POINTS_1 = 5,
        POINTS_2 = 3,
        POINTS_3 = 2,
        imageContainer,
        overallScore = 0,
        counter,
        landmarks,
        input,
        randomCity,
        boxes,
        image;
       
    function init(){
        landmarks = GeoQuizMain.Landmarks.getLandmarks();
        getElements();
        loadImage();
        setOverallScore();
        getUserInput();
    }
    
    
    
    function getUserInput(){
        input.addEventListener("keypress", getInput);  
    }
    
    /*check user input with correct anwer*/
    function getInput(event){
        var inputText = event.target.value.toLowerCase();
        if (event.keyCode === ENTER_KEY_CODE){
            checkAnswer(inputText);
        }
    }
    
    /*if answer is right set score*/
    function checkAnswer(inputText){
        if (inputText === randomCity.city){
            setScore();
            localStorage.setItem(randomCity.url, "correct");
            
        /*}else{
            localStorage.setItem(randomCity.url, "false");*/
        }
        input.value = "";
        removeNodes();
        loadImage();
    }
    
    function removeNodes(){
        var box;
        imageContainer.removeChild(image);
        box = document.querySelector(".boxes");
        imageContainer.removeChild(box); 
    }
    
    /*if score is not null get former score from local storage else set new score = 0;
    */
    function setOverallScore(){
        if (localStorage.getItem("score") != null){
            overallScore = localStorage.getItem("score");
        }else{
            overallScore = 0;
        }
        document.querySelector("#score").textContent = "score:  " + overallScore;
    }
    
    
    /*points depends on how much the user uncover the image. If answer is right user gets at least 2 points. For wrong answer 0
    */
    function setScore(){
        var points, 
            score;
        
        score = document.querySelector("#score");
        if (counter < 10){
            points = parseInt(POINTS_1);
            
        }
        if (counter >= 10 && counter < 20 ){
            points = parseInt(POINTS_2);
           
        }
        if(counter > 20){
            points= parseInt(POINTS_3);
            
        }
        overallScore = parseInt(overallScore) + parseInt(points);
        score.innerHTML = "Score: " + overallScore ;
        localStorage.setItem("score", overallScore);
    }
    
    /*uncover image if mouser is over hidden image*/
    function onMouseOver(){
        var i;
        boxes = document.getElementsByClassName("box");
        for (i=0; i < boxes.length; i++){
            boxes[i].addEventListener("mouseover",setOpacity);
        } 
    }
    
    function setOpacity(event){
        var target = event.target;
        target.style.opacity = 0;
        counter++;
    }
        
    
    /*get random city + image from array
    if answer is right, remove data from array
    game is finished if all questions are answered right
    */
    function loadImage(){
        var index;
        counter = 0;
        if(landmarks.length > 0){
            randomCity = landmarks[Math.floor(Math.random()*landmarks.length)];
            if(localStorage.getItem(randomCity.url)==="correct"){
                index = landmarks.indexOf(randomCity);
                if(index > -1){
                    landmarks.splice(index,1);
                    loadImage();
                }
            }else{
                setImage();
                createDivs();
                onMouseOver();  
            }   
        }
        if(landmarks.length === 0){
            setGameFinishedDialog();
        }
    }
        
    
    function setImage(){
        image = document.createElement("img");
        image.src = randomCity.url;
        imageContainer.appendChild(image);
    }
    
    function setGameFinishedDialog(){
        var dialog = document.querySelector("#success");
        dialog.style.visibility = "visible";  
    }
        
        
    
    /* cover image behind boxes*/
    function createDivs(){
        var i, boxes, box;
        boxes = document.createElement("div");
        boxes.classList.add("boxes");
        imageContainer.appendChild(boxes);
        for (i = 0; i < BOX_NUMBER; i++){
            box = document.createElement("div");
            box.classList.add("box");
            boxes.appendChild(box);
        } 
    }
    
    function getElements(){
        imageContainer = document.querySelector("#imageContainer");
        input = document.querySelector("input");
    }
    
   

    that.init = init;
    return that;
}());


