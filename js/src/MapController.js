var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.MapController = (function() {
    "use strict";
    /* eslint-env browser  */
    /*global Parse*/
    

    var that = {},
        PARSE_CONTINENTS = "Continents",
        PARSE_COUNTRIES = "Countries",
        PARSE_GERMANY = "Germany",
        PARSE_CONTINENT_ID = "continentsID",
        PARSE_COUNTRIES_ID = "countriesID",
        PARSE_GERMANY_ID = "germanyID",
        ENTER_KEY_CODE = 13,
        TIMEOUT_MAP = 1000,
        DISCOUNT_HINT = 2,
        DISCOUNT_FALSE = 5,
        TIMEOUT_POPUP = 1900,
        SCORE_UNLOCK_LEVEL_2 = 50,
        SCORE_UNLOCK_LEVEL_3 = 90,
        PARSE_NAME = "name",
        PARSE_NAME_2 = "name2",
        APPLICATION_ID = "OayYMfvwIZcSDhPOO7Z4eFPGGFYrNL4T1MMst80P",
        JAVASCRIPT_KEY = "oF5xeBWGnCcfOLP1S9VuSy7KlRD4E7WhgmRN22A2",
        
        
        location,
        parseObject,
        parseLocationId,
        query,
        currentLocation,
        currentLocation2,
        currentLocationId,
        locationArray =[],
        randomId,
        question,
        input,
        timer,
        overallScore = 0,
        call = 0,
        seconds,
        timerSound,
        success,
        levelText,
        quit,
        quit2,
        counter1 = 0,
        counter2 = 0;
      
    
    function init(){
        setCounter();
        setScore();
        setQuestion();
        getElements();
        timerSound = new Audio ("res/sounds/timer.mp3");
        setHintButtonEvent();
    }
    
    
    
   
    
    /*if score from local storage is not null, set former score, else set new score = 0*/
    function setScore(){
        if (localStorage.getItem("score") != null){
            overallScore = localStorage.getItem("score");
        }else{
            overallScore = 0;
        }
        document.querySelector("#score").textContent = "score:  " + overallScore;    
    }

    
    /*set Counter, so that the popup message for a new unlock level appears only once and not every time, if the user 
    opens a level 
    */
    function setCounter(){
        if(GeoQuizMain.geoquiz != undefined){
            if(localStorage.getItem("counter1") != null){
                counter1 = parseInt(localStorage.getItem("counter1"));   
            }
            counter1++;
            localStorage.setItem("counter1", counter1);
        }  
        if(GeoQuizMain.EuropeMap != undefined){
            if(localStorage.getItem("counter2") != null){
                counter2 = parseInt(localStorage.getItem("counter2"));
            }
            counter2++;
            localStorage.setItem("counter2", counter2);
        }
    }
    
   /*checks if score is reached to unlock level 2
    shows pop up message to inform user 
    */
    function level2Opened(){
        if(GeoQuizMain.geoquiz != undefined){
            if (localStorage.getItem("score")>SCORE_UNLOCK_LEVEL_2) { 
                if(parseInt(localStorage.getItem("counter1")) === 1 && !quit){
                    levelText = document.querySelector("#newLevel");
                    levelText.style.visibility = "visible";
                    quit = true;
                    setTimeout(function() {levelText.style.visibility = "hidden";}, TIMEOUT_POPUP);
                }
            }
        }
    }
    
 
    
    /*checks if score is reached to unlock level 3
    shows pop up message to inform user 
    */
    function level3Opened(){
        if(GeoQuizMain.EuropeMap != undefined) {
            if (localStorage.getItem("score")>SCORE_UNLOCK_LEVEL_3) {
                if(parseInt(localStorage.getItem("counter2")) === 1 && !quit2){
                    levelText = document.querySelector("#newLevel");
                    levelText.style.visibility = "visible";
                    quit2 = true;  
                    setTimeout(function() {levelText.style.visibility = "hidden";}, TIMEOUT_POPUP);
                } 
            }
        }
    }
    
   
    
    function getElements(){
        input = document.querySelector("#input");
    }
     
    /*check input value (different spelling possible: e.g. "Nordamerika" also accepted as "nord amerika") from user with rigt           answer: set new score and new question 
    */
    function onInputChanged(event){
        var inputText = event.target.value.toLowerCase();
        timerSound.pause();
        timerSound.currentTime = 0;
        if(event.keyCode=== ENTER_KEY_CODE){             
            if(inputText === currentLocation || inputText === currentLocation2){
                GeoQuizMain.MapView.locationSetColor(true, currentLocationId);
                localStorage.setItem(currentLocationId, "rightAnswered");
                pauseTimerTrue();
                setTimeout(function(){
                    GeoQuizMain.MapView.clickedLocation(currentLocationId);
                    call++;
                    getRandomId(); 
                    getLocation(randomId);                
                }, TIMEOUT_MAP);
                GeoQuizMain.MapView.setClick(currentLocationId);
            }else{
                GeoQuizMain.MapView.locationSetColor(false, currentLocationId);
                pauseTimerFalse();
                setTimeout(function(){
                    GeoQuizMain.MapView.clickedLocation(currentLocationId);
                    call++;
                    getRandomId(); 
                    getLocation(randomId);               
                }, TIMEOUT_MAP);  
                GeoQuizMain.MapView.setClick(currentLocationId);
            }
        }
                        
    }
    
    /*set timer: if less than 5 seconds left, set sound and timer color red
    */
    function setTime() {
        var timer;
        if (seconds > 0){
            seconds--;
            timer = document.querySelector("#timer");
            timer.innerHTML =  seconds;  
        }
        if (seconds === 5){
            setTimerColor();
            timerSound.play();    
        }
        if (seconds === 0){
            pauseTimerFalse();
            GeoQuizMain.MapView.setClick(currentLocationId);
            getRandomId(); 
            getLocation(randomId);
            GeoQuizMain.MapView.clickedLocation(currentLocationId);
            call++;
        }
    }
    
    function setTimerColor(){
        var timer = document.querySelector("#timer");
        timer.style.background = "#F78181";
    }
    
    /*substract 5 points if answer is wrong 
    */
    function pauseTimerFalse(){
        clearInterval(timer);
        overallScore = parseInt(overallScore) - parseInt(DISCOUNT_FALSE);
        document.querySelector("#score").textContent = "score:  " + overallScore;
        localStorage.setItem("score", overallScore);
    }
    
    /*add points from remaining time 
    */
    function pauseTimerTrue(){
        var currentTime;
        clearInterval(timer);
        currentTime = document.querySelector("#timer").textContent; 
        overallScore = parseInt(overallScore) + parseInt(currentTime);
        document.querySelector("#score").textContent = "score:  " + overallScore;
        localStorage.setItem("score", overallScore);
    }
    

    /*check answer and set new question
    */
    function checkAnswer(id){
        timerSound.pause();
        timerSound.currentTime = 0;
        if(id === currentLocationId){
            GeoQuizMain.MapView.locationSetColor(true, id);
            localStorage.setItem(id, "rightAnswered");
            pauseTimerTrue();
            setTimeout(function(){
                GeoQuizMain.MapView.clickedLocation(id);
                getRandomId(); 
                getLocation(randomId);   
                call++;

            }, TIMEOUT_MAP); 
        }else{
            GeoQuizMain.MapView.locationSetColor(false, id);
            GeoQuizMain.MapView.setClick(currentLocationId);
            pauseTimerFalse();
            setTimeout(function(){
                GeoQuizMain.MapView.clickedLocation(id);
                getRandomId(); 
                getLocation(randomId);   
                call++;

            }, TIMEOUT_MAP);
           
        }
    }
    
    function resetTimerBackground(){  
        var timer = document.querySelector("#timer");
        timer.style.background = "rgba(164,220,224,1)";
    }
    
    /*if all questions aren't still answered right, get new random question from parse
    if all questions answered right, show success message 
    */
    function getLocation(id){
        var index;
        if(locationArray.length > 0){
            query.get(id, {
                success: function(location){
                    currentLocation = location.get(PARSE_NAME);
                    currentLocation2 = location.get(PARSE_NAME_2);
                    currentLocationId = location.get(parseLocationId);
                    if (localStorage.getItem(currentLocationId) === "rightAnswered"){
                        index = locationArray.indexOf(id);
                        if (index > -1) {
                            locationArray.splice(index, 1);
                            getRandomId();
                            getLocation(randomId);                   
                        }    
                    }else{
                        newQuestion();   
                    }
                }
            });
        }   
        if(locationArray.length === 0){
            setSuccessDialog();
        }   
    }
    
    function setSuccessDialog(){
        success = document.querySelector("#overlay");
        success.style.visibility = "visible";
    }
    

    
    /*set question in turn: "input question" & "clickable question"
    */
    function newQuestion(){
        document.getElementById("user_input").value = "";
        level2Opened();
        level3Opened();
        GeoQuizMain.MapView.clearHintField();
        GeoQuizMain.MapView.removeHintData();
        seconds = 16;
        resetTimerBackground();
        timer = setInterval(setTime, TIMEOUT_MAP);                
        if((call%2) == 0){
            setQuestionText();         
        }else{
            GeoQuizMain.MapView.inputColor(currentLocationId); 
            setInputField();          
        }  
    }
    
    function setInputField(){
        input.addEventListener("input", onInputChanged);
        question.classList.add("hidden");
        input.classList.remove("hidden");
        input.addEventListener("keydown", onInputChanged);
        
    }
    
    function setQuestionText(){ 
        question = document.querySelector("#question_text");
        question.textContent = "Wo liegt " + currentLocation[0].toUpperCase() + currentLocation.slice(1) + "?";
        question.classList.remove("hidden");
        input.classList.add("hidden");
    }


    /*get random location stored in parse*/
    function getRandomId(){
        randomId = locationArray[Math.floor(Math.random()*locationArray.length)];
    }
    
    /*checks which if continentsmap, countriesmap or germanymap is opened and set variable values to get right parse objects
    */
    function setVariableValues(){
        if (GeoQuizMain.geoquiz != undefined){
            parseObject = PARSE_CONTINENTS;
            parseLocationId = PARSE_CONTINENT_ID;
        }
        if (GeoQuizMain.EuropeMap != undefined){
            parseObject = PARSE_COUNTRIES;
            parseLocationId = PARSE_COUNTRIES_ID;
        }
        if (GeoQuizMain.GermanyMap != undefined){
            parseObject = PARSE_GERMANY;
            parseLocationId = PARSE_GERMANY_ID;
        }
    }

    
   /*method gets IDs from parse and push them into array 
   get random ID from Array to display a question
   */
    function setQuestion(){
        var i, id;
        Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);
        setVariableValues();

        location = Parse.Object.extend(parseObject);
        query = new Parse.Query(location);        
        query.find({
            

            success:function(results) {
                
                for (i = 0; i < results.length; i++){
                    id = results[i].id;
                    locationArray.push(id);
                }
                getRandomId(); 
                GeoQuizMain.MapView.clearHintField();
                GeoQuizMain.MapView.removeHintData();
                getLocation(randomId);
                 
    
                
            }
        });

    }
    

    function setHintButtonEvent(){
        var hintButton = document.querySelector("#hint");
        hintButton.addEventListener("click", onHintButtonClicked);
    }
    
    /*get different hints from parse and display them 
    */
    function onHintButtonClicked(){
        substractScore();
        if (call%2 === 0){
            query.get(randomId, {
                success: function(location){
                    var hintText = location.get("hintText");
                    if (hintText != undefined){
                        GeoQuizMain.MapView.displayHintText(hintText);
                    }
                } 
            });   
        }else{
            query.get(randomId, {
                success: function(location){
                    var hintData = location.get("hint");
                    if (hintData != undefined){
                        GeoQuizMain.MapView.displayHintData(hintData);
                    }
                }//,
            });   
        }
    }
    
    /*substract 2 points if user gets hint
    */
    function substractScore(){ 
        overallScore = parseInt(overallScore) - parseInt(DISCOUNT_HINT);
        document.querySelector("#score").textContent = "score:  " + overallScore;
    }
    
    
    
    that.checkAnswer = checkAnswer;
    that.init = init;
    return that;
}());
