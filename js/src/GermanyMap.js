var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.GermanyMap = (function () {
    "use strict";
    
     /* eslint-env browser  */  
    /*global AmCharts */
    

    var that = {},
        mapController,
        map;
    
    function init(){
        loadMap();
        mapController = GeoQuizMain.MapController;

    }
    
    /*set color to location which should be guess*/
    function inputColor(id){
        var colorInput = "#ffffff";
        var area = map.getObjectById(id);
        area.color = colorInput;
        area.colorReal = area.color;
        area.selectable = false;
        map.returnInitialColor(area);
    }
    
    /*set maparea clickable*/
    function setClick(id){
        var area = map.getObjectById(id);
        area.selectable = true;
        
    }
    
    function clickedLocation(id){
        var colotInitial = "rgba(38, 64, 53, 1)";
        var area = map.getObjectById(id);
        area.color = colotInitial;
        area.colorReal = area.color;
        map.returnInitialColor(area);
    }
    
    /*color area for right answer green and for wrong answer red*/
    function setColor(right, id){
        var colorRight,
            colorWrong,
            area;
        
        colorRight = "#9FF781";
        colorWrong = "#F5A9A9";
        
        if(right === true){
            area = map.getObjectById(id);
            area.color = colorRight;
            area.colorReal = area.color;
            map.returnInitialColor(area);
        }
        if(right === false){            
            area = map.getObjectById(id);
            area.color = colorWrong;
            area.colorReal = area.color;
            map.returnInitialColor(area);
        }
    }
    
    
    function loadMap(){
        var dataProvider;
        
        AmCharts.ready( function() {
            map = new AmCharts.AmMap();
            map.panEventsEnabled = true;
            map.backgroundColor = "rgba(164,220,224,1)";
            map.backgroundAlpha = 1;

            dataProvider = {
                map: "germanyLow",
                getAreasFromMap: true
            };

            map.dataProvider = dataProvider;

            map.areasSettings = {
                color: "rgba(38,64,53,1)",
                colorSolid: "#5EB7DE",
                selectedColor: "#ffffff",
                outlineColor: "#ffffff",
                rollOverColor: "#F5DEB3",
                rollOverOutlineColor: "#FFFFFF",
                selectable: true
            };

            map.addListener("clickMapObject", function(event) {
                playSound();
                mapController.checkAnswer(event.mapObject.id);
                //mapView.checkAnswerEurope(event.mapObject.id);
            });
    
            map.export = {
                enabled: true
            };

            map.write( "chartdiv" );
        });
    }
    
    /*play Sound if area is selected*/
    function playSound(){
        var clickSound = new Audio("res/sounds/click.mp3");
        clickSound.play();
    }
    
    that.setClick = setClick;
    that.inputColor = inputColor;
    that.clickedLocation = clickedLocation;
    that.setColor = setColor; 
    that.init = init;
    return that;
}());

