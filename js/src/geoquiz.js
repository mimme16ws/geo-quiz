var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.geoquiz = (function () {
    "use strict";
    
    /* eslint-env browser  */
    /*global AmCharts*/
    
    var that = {},      
        mapController,
        map,
        dataProvider,
        area;
    
    function init(){        
        mapController = GeoQuizMain.MapController;        
        loadMap(); 
        
    }
    
    /*when a user has to give an input the location color is set to white*/
    function inputColor(id){
        var colorInput = "#ffffff";
        area = map.getObjectById(id);
        area.color = colorInput;
        area.colorReal = area.color;
        area.selectable = false;
        map.returnInitialColor(area);
    }
    
    function setClick(id){
        area = map.getObjectById(id);
        area.selectable = true;        
    }
    
    /*set the location color back to the initial color*/
    function clickedLocation(id){
        var colotInitial = "rgba(38, 64, 53, 1)";
        var area = map.getObjectById(id);
        area.color = colotInitial;
        area.colorReal = area.color;
        map.returnInitialColor(area);
    }
    
    /*set the color to the locations depending if it was right or wrong answered*/
    function setColor(right, id){
        var colorRight,
            colorWrong,
            area;
        
        colorRight = "#9FF781";
        colorWrong = "#F5A9A9";
        
        if(right === true){
            area = map.getObjectById(id);
            area.color = colorRight;
            area.colorReal = area.color;
            map.returnInitialColor(area);
        }
        if(right === false){            
            area = map.getObjectById(id);
            area.color = colorWrong;
            area.colorReal = area.color;
            map.returnInitialColor(area);
        }
    }
    
    /*load the map from amcharts and set a click listener on the locations*/
    function loadMap(){
        AmCharts.ready( function() {
            map = new AmCharts.AmMap();
            map.panEventsEnabled = true;
            map.backgroundColor = "rgba(164,220,224,1)";
            map.backgroundAlpha = 1;

            dataProvider = {
                map: "continentsLow",
                getAreasFromMap: true
            };

            map.dataProvider = dataProvider;
    
            map.areasSettings = {
                autoZoom: false,
                color: "rgba(38,64,53,1)",
                colorSolid: "#5EB7DE",
                selectedColor: "#000000",
                outlineColor: "#ffffff",
                rollOverColor: "#F5DEB3",
                rollOverOutlineColor: "#FFFFFF",
                selectable: true
            };

            map.addListener("clickMapObject", function(event) {
                playSound();
                mapController.checkAnswer(event.mapObject.id);
        
            });
            map.export = {
                enabled: true
            };
            map.write("chartdiv");
        });
    }
    
    /*play sound if a location is clicked*/
    function playSound(){
        var clickSound = new Audio("res/sounds/click.mp3");
        clickSound.play();
    }
    
    that.setClick = setClick;
    that.inputColor = inputColor;
    that.clickedLocation = clickedLocation;
    that.setColor = setColor;
    that.init = init;
    return that;
}());
