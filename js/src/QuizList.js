var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.QuizList = (function () {
    "use strict";
    
    /* eslint-env browser  */



    var that = {},
        questions = [
            {
                "question":"In welchem Land wird portugiesisch gesprochen?",
                "option1":"Brasilien",
                "option2":"Argentinien",
                "option3":"Ecuador",
	            "option4":"Schweden",
                "rightAnswer":"one"
                
            },
	        {
                
                "question":"Welcher Kontinent hat die meisten Einwohner?",
                "option1":"Europa",
                "option2":"Nord Amerika",
                "option3":"Asien",
	            "option4":"Australien",
                "rightAnswer":"three"
            },
	        {
                "question":"Welches ist der längste Fluss in Europa?",
                "option1":"Wolga",
                "option2":"Rhein",
                "option3":"Donau",
	            "option4":"Dnepr",
	            "rightAnswer":"one"
            },
            {
                "question":"Wie heißt die Hauptstadt von Norwegen?",
                "option1":"Oslo",
                "option2":"Stockholm",
                "option3":"Bergen",
                "option4":"Wien",
                "rightAnswer":"one"
            },
	        {
                
                "question":"Was ist das flächenmäßig größte Bundesland Deutschlands?",
                "option1":"Niedersachen",
                "option2":"Saarland",
                "option3":"Thüringen",
                "option4":"Bayern",
                "rightAnswer":"four"
            },
	        {
                "question":"Welches Land Europas ist am dünnsten besiedelt?",
                "option1":"Norwegen",
                "option2":"Schweden",
                "option3":"Finnland",
	            "option4":"Island",
                "rightAnswer":"four"
            },
            {
                "question":"Welcher Fluss fließt durch Prag?",
                "option1":"Elbe",
                "option2":"Donau",
                "option3":"Moldau",
	            "option4":"Seine",
	            "rightAnswer":"three"
            },
	        {
                "question":"Auf welchem Kontinent liegt Indien?",
                "option1":"Afrika",
                "option2":"Asien",
                "option3":"Europa",
                "option4":"Süd Amerika",
                "rightAnswer":"two"
            },
            {
                "question":"Welches ist der längste Fluss der Welt?",
                "option1":"Amazonas",
                "option2":"Nil",
                "option3":"Mississippi",
                "option4":"Jangtsekian",
	            "rightAnswer":"two"
            },
	        {
                "question":"Wie heißt der höchste noch tätige Vulkan Europas?",
                "option1":"Hekla",
                "option2":"Stromboli",
                "option3":"Ätna",
                "option4":"Vesuv",
	            "rightAnswer":"three"
            },
	        {
                "question":"Wie viele Bundesländer gibt es in Österreich?",
                "option1":"12",
                "option2":"9",
                "option3":"3",
	            "option4":"6",
                "rightAnswer":"two"
            },
	        {
                "question":"In welcher amerikanischen Stadt wird die Straßenbahn von Kabeln gezogen?",
                "option1":"New York",
                "option2":"Los Angeles",
                "option3":"Chicago",
	            "option4":"San Francisco",
	            "rightAnswer":"four"
            },
	        {
                "question":"Welches ist die goldene Stadt?",
                "option1":"Paris",
                "option2":"London",
                "option3":"Prag",
                "option4":"New York",
	            "rightAnswer":"three"
            },
            {
                "question":"Welches ist die größte Insel der Erde?",
                "option1":"Borneo",
                "option2":"Neuguinea",
                "option3":"Grönland",
                "option4":"Madagaskar",
	            "rightAnswer":"three"
            },
	        {
                
                "question":"Welches ist der größte bayerische See?",
                "option1":"Chiemsee",
                "option2":"Starnberger See",
                "option3":"Ammersee",
	            "option4":"Walchensee",
                "rightAnswer":"one"
            },
            {
                
                "question":"Welches ist das flächenmäßig größte Bundesland der USA?",
                "option1":"Kalifornien",
                "option2":"Alaska",
                "option3":"South Carolina",
                "option4":"Texas",
                "rightAnswer":"two"
                
            },
            {
                
                "question":"Wie heißt die Hauptstadt Lybiens?",
                "option1":"Tripolis",
                "option2":"Beirut",
                "option3":"Amman",
                "option4":"Algier",
                "rightAnswer":"one"
                
            },
	        {
                
	            "question":"Was ist der tiefste See?",
                "option1":"Malawisee",
                "option2":"Großer Sklavensee",
                "option3":"Vansee",
	            "option4":"Baikalsee",
	            "rightAnswer":"four"
            },
            {
                "question":"Von welcher Stadt Brasiliens ist die Christusstatue Cristo Redentor das Wahrzeichen?",
                "option1":"Rio de Janeiro",
                "option2":"Porto Alegre",
                "option3":"Sao Paulo",
                "option4":"Belo Horizonte",
                "rightAnswer":"one"
            },
	        {
                
                "question":"Wie lang ist der Äuquator?",
                "option1":"25.000km",
                "option2":"40.000km",
                "option3":"60.000km",
                "option4":"75.000km",
                "rightAnswer":"two"
            },
            {
                
                "question":"Welches Land ist lang und dünn?",
                "option1":"Bolivien",
                "option2":"Uruguay",
                "option3":"Colombien",
	            "option4":"Chile",
	            "rightAnswer":"four"
            }
      ];
    
    function getQuestions (){
        return questions;
    }
        
    
   
    
    
    that.getQuestions = getQuestions;
    return that;
}());
