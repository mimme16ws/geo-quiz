var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.quiz = (function () {
    "use strict";
    
    /* eslint-env browser  */

    
    var that = {},
        questionArray = new Array(),
        numberOfQuestions,
        questionNumber = 0,
        rightAnswer,
        localScore,
        closeButton,
        success,
        answerOne,
        answerTwo, 
        answerThree, 
        answerFour,
        feedback,
        randomArray,
        answerScores = 5;
    
    function init(){
        displayScore();
        getQuestionsData();
        //requestJsonData();        
    }
    
    /*set the value "score" of the localStorage to the screen*/
    function displayScore(){        
        if (localStorage.getItem("score") != null){
            localScore = localStorage.getItem("score");
        }
        else{
            localScore = 0;
        }
        document.querySelector("#score").textContent = "Score: " + localScore;
    }
    
    /*display a feedback for a correct or a wrong answer*/
    function setFeedback(status){
        if (status == "right"){
            feedback = document.querySelector(".feedback1");
            feedback.classList.remove("hidden");
            setTimeout(function(){
                feedback.classList.add("hidden");           
            }, 1000); 
        }if (status == "wrong"){
            feedback = document.querySelector(".feedback2");
            feedback.classList.remove("hidden");
            setTimeout(function(){
                feedback.classList.add("hidden");           
            }, 1000); 
        }
        
    }
    
    /*check if the question was right answered and sets the corresponding score*/
    function checkQuestion(data){
        var currentId;
        currentId = data.target.id;
        if(currentId == rightAnswer){
            localScore = parseInt(localScore) + parseInt(answerScores);
            localStorage.setItem("score", localScore);
            displayScore();
            setFeedback("right");            
        }
        else{
            localScore = parseInt(localScore) - parseInt(answerScores);
            localStorage.setItem("score", localScore);
            displayScore();
            setFeedback("wrong");             
        }
        questionNumber++;
        numberOfQuestions--;
        showQuestion();
    }
     
    /*set the question and the answer options to the screen,
     if all questions are answered a message appears*/
    function showQuestion(){
        if(numberOfQuestions > 0){
            document.querySelector(".questionText").textContent = questionArray[questionNumber][0];
            answerOne = document.querySelector("#one");
            answerOne.textContent = questionArray[questionNumber][1];
            answerOne.addEventListener("click", checkQuestion);
            answerTwo = document.querySelector("#two");
            answerTwo.textContent = questionArray[questionNumber][2];
            answerTwo.addEventListener("click", checkQuestion);
            answerThree = document.querySelector("#three");
            answerThree.textContent = questionArray[questionNumber][3];
            answerThree.addEventListener("click", checkQuestion);
            answerFour = document.querySelector("#four");
            answerFour.textContent = questionArray[questionNumber][4];
            answerFour.addEventListener("click", checkQuestion);        
            rightAnswer = questionArray[questionNumber][5];
        }
        else{
            success = document.querySelector("#overlay");
            success.style.visibility = "visible";            
            closeButton = document.querySelector(".close");
            closeButton.addEventListener("click", onCloseButtonClicked); 
        }                             
    }
    
    function onCloseButtonClicked(){
        success.style.visibility ="hidden";        
    }
    
    /*shuffle the array from the json/js to mix the questions*/
    function shuffle(randomArray) {
        var j, x, i;
        for (i = randomArray.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = randomArray[i - 1];
            randomArray[i - 1] = randomArray[j];
            randomArray[j] = x;
        }
    }
    
    /*get the questions array from the QuizList.js*/
    function getQuestionsData(){
        var i;
        randomArray = GeoQuizMain.QuizList.getQuestions();
        shuffle(randomArray);
        for(i = 0; i < randomArray.length; i++){
            questionArray[i] = new Array;
            questionArray[i][0] = randomArray[i].question;
            questionArray[i][1] = randomArray[i].option1;
            questionArray[i][2] = randomArray[i].option2;
            questionArray[i][3] = randomArray[i].option3;
            questionArray[i][4] = randomArray[i].option4;
            questionArray[i][5] = randomArray[i].rightAnswer;
        }   
        numberOfQuestions = questionArray.length;
        showQuestion();        
    }
    
    //Aufruf der JSON-Datei erzeugte bei manchen Chrome-Aufrufen Fehlermeldungen (jquery.js:5 XMLHttpRequest cannot load file:///C:/Users/Alice/Documents/geo-quiz/quizfragen.json. Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https, chrome-extension-resource.), weshalb die Datei in eine JS-Datei gelagert wurde und durch einer GET-Methode geholt wird
    //Ajax Aufruf von lokaler json Datei geht in Chrome nicht, deshalb jquery
    //entsprechender Code für den Json Aufruf via jquery wäre wie folgt
    /*function requestJsonData(){
        var i;
    $.getJSON("quizfragen.json", function (data){ 
        randomArray = data.quizlist;
        shuffle(randomArray);        
        for(i = 0; i < randomArray.length; i++){
            questionArray[i] = new Array;
            questionArray[i][0] = data.quizlist[i].question;
            questionArray[i][1] = data.quizlist[i].option1;
            questionArray[i][2] = data.quizlist[i].option2;
            questionArray[i][3] = data.quizlist[i].option3;
            questionArray[i][4] = data.quizlist[i].option4;
            questionArray[i][5] = data.quizlist[i].rightAnswer;
        }   
        numberOfQuestions = questionArray.length;
        showQuestion();
})
    }*/  
    
    that.init = init;
    return that;
}());
