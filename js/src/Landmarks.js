var GeoQuizMain = GeoQuizMain || {};
GeoQuizMain.Landmarks= (function() {
    "use strict";
    
    /* eslint-env browser  */

    
    var that = {},
        landmarks = [
            {
                "city": "new york",
                "url": "res/images/landmarks/statue_of_liberty.jpg"
            },
            {
                "city": "athen",
                "url": "res/images/landmarks/acropolis.jpg"
                
            },
            {
                "city": "sydney",
                "url": "res/images/landmarks/sydney_opera_house.jpg"
            },
            {
                "city": "regensburg",
                "url": "res/images/landmarks/regensburg_dom.jpg"
                
            },
            {
                "city": "paris",
                "url": "res/images/landmarks/eiffeltower.jpg"
            },
            {
                "city": "berlin",
                "url": "res/images/landmarks/brandenburger_tor.jpg"
            },
            {
                "city": "london",
                "url": "res/images/landmarks/big_ben.jpg"
            },
            {
                "city": "köln",
                "url": "res/images/landmarks/cologne_dom.jpg"
            },
            {
                "city": "rio de janeiro",
                "url": "res/images/landmarks/cristo_redentor.jpg"
            
            },
            {
                "city": "pisa",
                "url": "res/images/landmarks/pisa_tower.jpg"
            },
            {
                "city": "washington",
                "url": "res/images/landmarks/washington_monument.jpg"
            }
            
        ];
    
    /*return array with citynames + image-url*/
    function getLandmarks(){
        return landmarks;  
    }
        
        
       
   
    that.getLandmarks = getLandmarks;
    return that;
}());







